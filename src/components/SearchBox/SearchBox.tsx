import {
  Image,
  Pressable,
  StyleSheet,
  TextInput,
  TextInputProps,
  View,
} from 'react-native';
import Conditional from '../Conditional/Conditional';
import Icons from '../../assets/icons';
type SearBoxProps = TextInputProps & {
  onClear: () => void;
};

const SearchBox = (props: SearBoxProps) => {
  const {onClear, value} = props ?? {};
  const handleClearPress = () => {
    if (onClear) {
      onClear();
    }
  };

  return (
    <View style={styles.searchContainer}>
      <View style={styles.inputContainer}>
        <Image source={Icons.search} style={styles.searchIcon} />
        <TextInput style={styles.input} autoCorrect={false} {...props} />
        <Conditional renderIf={!!value}>
          <Pressable
            onPress={handleClearPress}
            style={styles.clearIconContainer}
            hitSlop={8}>
            <Image source={Icons.clear} style={styles.clearIcon} />
          </Pressable>
        </Conditional>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  searchContainer: {height: 44},
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 1,
    alignItems: 'center',
    borderColor: '#797670',
    borderRadius: 4,
    backgroundColor: '#383733',
  },
  input: {
    height: 40,
    color: '#FFFFFF',
    width: '100%',
    fontFamily: 'PP Neue Montreal',
  },
  searchIcon: {height: 20, width: 20, marginHorizontal: 8},
  clearIconContainer: {position: 'absolute', right: 8},
  clearIcon: {height: 12, width: 12},
});

export default SearchBox;

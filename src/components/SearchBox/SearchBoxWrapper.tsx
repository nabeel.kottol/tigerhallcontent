import {useEffect, useState} from 'react';
import SearchBox from './SearchBox';
import {useDebounce} from '../../hooks/useDebounce/useDebounce';

type SearchBoxWrapperProps = {
  onKeywordChange: (keyword: string) => void;
};
const SearchBoxWrapper = ({onKeywordChange}: SearchBoxWrapperProps) => {
  const [query, setQuery] = useState('');
  const onChangeText = (text: string) => setQuery(text);
  const searchKeyword = useDebounce<string>(query);

  useEffect(() => {
    onKeywordChange(searchKeyword);
  }, [onKeywordChange, searchKeyword]);

  const handleOnClear = () => {
    setQuery('');
  };

  return (
    <SearchBox
      onClear={handleOnClear}
      value={query}
      onChangeText={onChangeText}
      placeholder="Search podcasts..."
      placeholderTextColor={'#fff'}
    />
  );
};

export default SearchBoxWrapper;

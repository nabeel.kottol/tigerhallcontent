import {ReactNode} from 'react';

export default function Conditional({
  renderIf,
  children,
}: {
  renderIf?: boolean | string | number;
  children?: ReactNode;
}) {
  if (renderIf) {
    return <>{children}</>;
  }

  return <></>;
}

import {ActivityIndicator, FlatList} from 'react-native';
import {ContentType} from '../../screens/HomeScreen';
import ContentItem from './ContentItem';

type ContentListProps = {
  contents: {
    edges: Array<ContentType>;
    meta: {
      limit: number;
      offset: number;
      total: number;
    };
  };
  isLoadingMore: boolean;
  onLoadMore: (offset: number) => void;
};

const ContentList = ({
  contents,
  isLoadingMore,
  onLoadMore,
}: ContentListProps) => {
  const itemKeyExtractor = (item: ContentType, index: number) => {
    return `${index}${item.name}`;
  };

  const loadMoreContents = () => {
    const {total} = contents?.meta ?? {};
    const newOffset = contents?.edges?.length;
    const hasMoreData = total > newOffset;

    if (hasMoreData) {
      onLoadMore(newOffset);
    }
  };

  return (
    <FlatList
      data={contents?.edges}
      renderItem={({item}) => <ContentItem content={item} />}
      keyExtractor={itemKeyExtractor}
      onEndReached={loadMoreContents}
      onEndReachedThreshold={0.1}
      ListFooterComponent={
        isLoadingMore ? (
          <ActivityIndicator size="large" color={'#FF7429'} />
        ) : null
      }
    />
  );
};

export default ContentList;

import {Image, StyleSheet, Text, View} from 'react-native';
import {ContentType} from '../../screens/HomeScreen';
import Icons from '../../assets/icons';
import {getLowResImage} from '../../helpers/image';
type ContentItemProps = {
  content: ContentType;
};

const ContentItem = ({content}: ContentItemProps) => {
  const {categories, experts, image, name} = content ?? {};

  return (
    <View style={styles.card}>
      <View>
        <Image
          source={{uri: getLowResImage(image.uri)}}
          style={styles.contentMainImage}
        />

        <View style={styles.progressContainer}>
          <Image source={Icons.pieChart} style={styles.pieChartIcon} />
          <Text style={styles.progressText}>30% Completed</Text>
        </View>

        <View style={styles.durationContainer}>
          <Image source={Icons.clock} style={styles.clockIcon} />
          <Text style={styles.durationText}>20m</Text>
        </View>

        <View style={styles.headsetIconContainer}>
          <Image source={Icons.headset} style={styles.headsetIcon} />
        </View>
      </View>
      <View style={styles.progressBarContainer}>
        <View style={styles.progressBar} />
      </View>
      <View style={styles.contentDetailsContainer}>
        <View>
          <Text style={styles.categoryText}>{categories?.[0]?.name}</Text>
        </View>
        <View style={styles.contentNameContainer}>
          <Text style={styles.contentNameText}>{name}</Text>
        </View>

        <View style={styles.expertNameContainer}>
          <View>
            <Text
              style={
                styles.expertNameText
              }>{`${experts?.[0]?.firstName} ${experts?.[0]?.lastName}`}</Text>
          </View>

          <View>
            <Text style={styles.companyText}>
              {experts?.[0]?.company || 'Company'}
            </Text>
          </View>
        </View>

        <View style={styles.actionContainer}>
          <Image source={Icons.share} style={styles.shareIcon} />
          <Image source={Icons.bookmark} style={styles.bookmarkIcon} />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  bookmarkIcon: {height: 16, width: 16, marginLeft: 12},
  shareIcon: {height: 16, width: 16},
  actionContainer: {flexDirection: 'row', justifyContent: 'flex-end'},
  companyText: {
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 14.4,
    color: '#797670',
    fontFamily: 'PP Neue Montreal',
  },
  expertNameText: {
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 14.4,
    color: '#4D4B46',
    fontFamily: 'PP Neue Montreal',
  },
  expertNameContainer: {marginTop: 8},
  contentNameText: {
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 19.2,
    color: '#000000',
    fontFamily: 'PP Neue Montreal',
  },
  contentNameContainer: {marginTop: 4},
  categoryText: {
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 14.4,
    color: '#797670',
    fontFamily: 'PP Neue Montreal',
  },
  contentDetailsContainer: {paddingHorizontal: 8, paddingVertical: 12},
  headsetIcon: {height: 16, width: 16},
  headsetIconContainer: {
    backgroundColor: '#FF5900',
    position: 'absolute',
    left: 8,
    bottom: 12,
    padding: 6,
    borderRadius: 14,
    justifyContent: 'center',
    alignItems: 'center',
  },
  durationText: {
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 14.4,
    color: '#FFFFFF',
    fontFamily: 'PP Neue Montreal',
  },
  clockIcon: {height: 12, width: 12, marginRight: 4},
  durationContainer: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    position: 'absolute',
    right: 8,
    bottom: 12,
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderRadius: 12,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  progressText: {
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 14.4,
    color: '#383733',
    fontFamily: 'PP Neue Montreal',
  },
  pieChartIcon: {height: 12, width: 12, marginRight: 4},
  progressContainer: {
    backgroundColor: '#FFF9F6',
    position: 'absolute',
    left: 0,
    top: 0,
    padding: 4,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentMainImage: {height: 120},
  card: {
    marginVertical: 8,
    backgroundColor: 'white',
    borderRadius: 8,
    overflow: 'hidden',
  },
  progressBarContainer: {width: '100%', backgroundColor: '#DEDBD4', height: 3},
  progressBar: {backgroundColor: '#FF5900', width: '30%', height: 3},
});

export default ContentItem;

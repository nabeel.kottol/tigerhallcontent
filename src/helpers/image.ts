export const getLowResImage = (uri: string) => {
  if (!uri) {
    return '';
  }

  return uri.replace(
    'https://images.staging.tigerhall.io/',
    'https://images.staging.tigerhall.io/resize/250x/',
  );
};

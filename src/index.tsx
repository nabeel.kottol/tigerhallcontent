import {ApolloProvider} from '@apollo/client';
import React from 'react';
import apolloClient from './graphql/client';
import HomeScreen from './screens/HomeScreen';

function App(): JSX.Element {
  return (
    <ApolloProvider client={apolloClient}>
      <HomeScreen />
    </ApolloProvider>
  );
}

export default App;

import {gql} from '@apollo/client';

export const PODCASTS_QUERY_USING_KEYWORD = gql`
  query GetPodcasts($keywords: String!, $offset: Int) {
    contentCards(
      filter: {
        limit: 10
        keywords: $keywords
        types: [PODCAST]
        offset: $offset
      }
    ) {
      edges {
        ... on Podcast {
          name
          image {
            ...Image
          }
          categories {
            ...Category
          }
          experts {
            ...Expert
          }
        }
      }
      meta {
        total
        limit
        offset
      }
    }
  }

  fragment Image on Image {
    uri
  }

  fragment Category on Category {
    name
  }

  fragment Expert on Expert {
    firstName
    lastName
    title
    company
  }
`;

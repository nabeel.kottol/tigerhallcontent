import {ApolloClient, InMemoryCache} from '@apollo/client';

export default new ApolloClient({
  uri: 'https://api.staging.tigerhall.io/graphql',
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          contentCards: {
            keyArgs: ['filter', ['keywords']],
            merge(existing = {edges: []}, incoming = {}, {args}) {
              const edges = existing?.edges ? existing.edges.slice(0) : [];

              for (let i = 0; i < incoming.edges.length; ++i) {
                edges[args?.filter.offset + i] = incoming.edges[i];
              }
              const mergedCache = {
                ...incoming,
                edges,
              };
              return mergedCache;
            },
          },
        },
      },
    },
  }),
});

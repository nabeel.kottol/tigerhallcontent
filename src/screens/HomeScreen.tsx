import {NetworkStatus, useLazyQuery} from '@apollo/client';
import LottieView from 'lottie-react-native';
import {useCallback, useEffect, useState} from 'react';
import {SafeAreaView, StatusBar, StyleSheet, Text, View} from 'react-native';
import lotties from '../assets/lottie';
import Conditional from '../components/Conditional/Conditional';
import ContentList from '../components/ContentList/ContentList';
import SearchBoxWrapper from '../components/SearchBox/SearchBoxWrapper';
import {PODCASTS_QUERY_USING_KEYWORD} from '../graphql/queries';
type ImageType = {
  uri: string;
};

type CategoryType = {
  name: string;
};

type ExpertType = {
  firstName: string;
  lastName: string;
  title: string;
  company: string;
};
type Podcast = {
  name: string;
  image: ImageType;
  categories: Array<CategoryType>;
  experts: Array<ExpertType>;
};

export type ContentType = Podcast;

const HomeScreen = () => {
  const [keyword, setKeyword] = useState('');
  const [getContents, {loading, error, data, fetchMore, networkStatus}] =
    useLazyQuery(PODCASTS_QUERY_USING_KEYWORD, {
      variables: {keywords: '', offset: 0},
      notifyOnNetworkStatusChange: true,
    });

  const isSearching = networkStatus === NetworkStatus.setVariables;
  const isErrorState = networkStatus === NetworkStatus.error;
  const isLoadingMore = networkStatus === NetworkStatus.fetchMore;
  const hasNoContent =
    !loading &&
    !error &&
    keyword &&
    (!data?.contentCards?.edges || data?.contentCards.edges.length === 0);

  useEffect(() => {
    getContents({
      variables: {keywords: keyword},
    });
  }, [getContents, keyword]);

  const onKeywordChange = useCallback((text: string) => setKeyword(text), []);

  const handleLoadMoreContent = (offset: number) => {
    fetchMore({
      variables: {offset},
    });
  };

  return (
    <SafeAreaView style={styles.parentContainer}>
      <StatusBar />
      <View style={styles.mainContainer}>
        <SearchBoxWrapper onKeywordChange={onKeywordChange} />
        <View style={styles.listContainer}>
          <Conditional renderIf={isSearching}>
            <LottieView
              source={lotties.search}
              autoPlay
              loop
              style={styles.searchLottie}
            />
          </Conditional>
          <Conditional renderIf={isErrorState}>
            <View style={styles.messageContainer}>
              <LottieView
                source={lotties.error}
                autoPlay
                loop={false}
                style={styles.errorLottie}
              />
              <Text style={styles.messageText}>Something went wrong</Text>
            </View>
          </Conditional>

          <Conditional renderIf={hasNoContent}>
            <View style={styles.messageContainer}>
              <LottieView
                source={lotties.noData}
                autoPlay
                loop={false}
                style={styles.noDataLottie}
              />
              <Text style={styles.messageText}>
                {`Sorry, no content for '${keyword}'`}
              </Text>
            </View>
          </Conditional>

          <Conditional renderIf={data?.contentCards}>
            <ContentList
              contents={data?.contentCards}
              onLoadMore={handleLoadMoreContent}
              isLoadingMore={isLoadingMore}
            />
          </Conditional>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  parentContainer: {backgroundColor: 'black', flex: 1},
  mainContainer: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 16,
    flexDirection: 'column',
  },
  listContainer: {marginTop: 20, flex: 1},
  messageText: {color: 'white', fontSize: 18},
  messageContainer: {flex: 1, alignItems: 'center', padding: 16},
  errorLottie: {height: 100, width: '100%', marginBottom: 16},
  noDataLottie: {height: 200, width: '100%'},
  searchLottie: {height: 400},
});

export default HomeScreen;

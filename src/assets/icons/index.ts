export default {
  bookmark: require('./bookmark/bookmark.png'),
  clear: require('./clear/clear.png'),
  clock: require('./clock/clock.png'),
  headset: require('./headset/headset.png'),
  pieChart: require('./pie-chart/pie-chart.png'),
  search: require('./search/search.png'),
  share: require('./share/share.png'),
};

export default {
  search: require('./search.json'),
  noData: require('./no-data.json'),
  error: require('./error.json'),
};
